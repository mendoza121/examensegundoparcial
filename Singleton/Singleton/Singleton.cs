﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Singleton
{
    class Singleton
    {
        // contiene la instancia singleton.
        private static Singleton Instance;
        public string text;

        // constructor privado para que la propia clase Singleton pueda llamarse a sí misma.
        private Singleton(string text)
        {
            this.text = text;
        }

        public static Singleton GetInstance(string text)
        {
            //comprueba si se crea la instancia
            if (Instance == null)
            {
                // si es null crear uno
                Instance = new Singleton(text);
            }
            // devolver la instancia creada en cada llamada posterior
            return Instance;
        }
    }
}
