﻿using System;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            //Nombre: Jheison Mendoza Asevedo
            //Fecha: 20/01/2022
            //Curso: Cuarto Nivel
            //Paraleo: "C"

            //Enunciado de lo que se hizo
            Console.WriteLine("Demuestra la clase Singleton. La salida de las dos declaraciones impresas a continuación será la misma");
            Console.WriteLine("porque la clase devolvió la instancia almacenada en caché de la segunda llamada.");

            //Instacias de Singleton
            Singleton one = Singleton.GetInstance("Jheison");
            Singleton two = Singleton.GetInstance("Mendoza");
            Singleton Three = Singleton.GetInstance("Asevedo");
            Singleton Four = Singleton.GetInstance("Cuarto Semestre");
            Singleton Five = Singleton.GetInstance("Paralelo 'c'");

            //Mostrar en Pantalla de Consola
            Console.WriteLine(one.text);
            Console.WriteLine(two.text);
            Console.WriteLine(Three.text);
            Console.WriteLine(Four.text);
            Console.WriteLine(Five.text);
        }
    }
}
